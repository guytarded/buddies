﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Buddies.Data_Access;

namespace Buddies.Models
{
    public class SearchValues
    {
        public String UserName { get; set; }
        public IEnumerable<SelectListItem> Landen { get; set; }
        public IEnumerable<SelectListItem> Geslacht { get; set; }
        public DateTime GeboorteDatum { get; set; }
        public IEnumerable<SelectListItem> Sterrenbeeld { get; set; }
        public String Beroep { get; set; }
        public IEnumerable<SelectListItem> BurgerlijkeStaat { get; set; }
        public IEnumerable<SelectListItem> StudieNiveau { get; set; }
        public String Grootte { get; set; }
        public String Gewicht { get; set; }
        public String Hobbys { get; set; }

        public SearchValues()
        {
            SelectListDAO _dao = new SelectListDAO();
            ProfileDAO _profileDao = new ProfileDAO();

            this.Landen = _dao.getCountries();
            this.Geslacht = _dao.getGeslachten();
            this.Sterrenbeeld = _dao.getSterrenbeelden();
            this.BurgerlijkeStaat = _dao.getBurgelijkeStaten();
            this.StudieNiveau = _dao.getStudieNiveaus();
        }
    }
}