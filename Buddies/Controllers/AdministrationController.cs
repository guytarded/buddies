﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Buddies.Data_Access;

namespace Buddies.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdministrationController : Controller
    {
        // Default administrationpage.
        // GET: Administration
        public ActionResult Index()
        {
            var dao = new ProfileDAO();
            return View(dao.GetAllProfiles());
        }

        /// <summary>
        /// Toggle the active status
        /// </summary>
        /// <param name="collection"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ToggleActivation(FormCollection collection)
        {
            var dao = new ProfileDAO();

            // update
            var result = dao.ToggleProfileActivation(collection["userID"]);

            // create the view and display the proper message
            ViewData.Add("Message", string.Empty);
            ViewBag.Message = result == 1 ? "De status is succesvol bijgewerkt" : "Er is iets misgegaan, probeer opnieuw";

            return View();
        }
    }
}