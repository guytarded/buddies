﻿using Buddies.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Buddies.Data_Access
{
    public class SelectListDAO
    {
        public IEnumerable<SelectListItem> getCountries()
        {

            BuddiesDataContext dc = new BuddiesDataContext();

            IEnumerable<SelectListItem> myList = from landen in dc.bud_Landens
                                                 select new SelectListItem
                                                 {
                                                     Text = landen.Land,
                                                     Value = landen.Land_ID.ToString()
                                                 };

            myList = myList.OrderBy(p => p.Text).ToList();

            return myList;
        }

        public IEnumerable<SelectListItem> getCountries(String id)
        {

            BuddiesDataContext dc = new BuddiesDataContext();
            List<SelectListItem> myListNew = new List<SelectListItem>();

            IEnumerable<SelectListItem> myListOld = from landen in dc.bud_Landens
                                                    select new SelectListItem
                                                    {
                                                        Text = landen.Land,
                                                        Value = landen.Land_ID.ToString()
                                                    };

            foreach (SelectListItem item in myListOld)
            {
                if (item.Text == id)
                {
                    item.Selected = true;
                }

                myListNew.Add(item);

            }

            myListNew = myListNew.OrderBy(p => p.Text).ToList();

            return myListNew;
        }

        public IEnumerable<SelectListItem> getGeslachten()
        {

            BuddiesDataContext dc = new BuddiesDataContext();

            IEnumerable<SelectListItem> myList = from geslachten in dc.bud_Geslachtens
                                                 select new SelectListItem
                                                 {
                                                     Text = geslachten.Geslacht,
                                                     Value = geslachten.Geslacht
                                                 };

            myList = myList.OrderBy(p => p.Text).ToList();

            return myList;
        }

        public IEnumerable<SelectListItem> getGeslachten(String geslacht)
        {
            BuddiesDataContext dc = new BuddiesDataContext();
            List<SelectListItem> myListNew = new List<SelectListItem>();

            IEnumerable<SelectListItem> myListOld = from geslachten in dc.bud_Geslachtens
                                                    select new SelectListItem
                                                    {
                                                        Text = geslachten.Geslacht,
                                                        Value = geslachten.Geslacht
                                                    };


            foreach (SelectListItem item in myListOld)
            {
                if (item.Text == geslacht)
                {
                    item.Selected = true;
                }

                myListNew.Add(item);

            }

            myListNew = myListNew.OrderBy(p => p.Text).ToList();

            return myListNew;
        }

        internal IEnumerable<SelectListItem> getSterrenbeelden()
        {
            BuddiesDataContext dc = new BuddiesDataContext();

            IEnumerable<SelectListItem> myList = from beelden in dc.bud_Sterrenbeeldens
                                                 select new SelectListItem
                                                 {
                                                     Text = beelden.Sterrenbeeld,
                                                     Value = beelden.Sterrenbeeld
                                                 };

            myList = myList.OrderBy(p => p.Text).ToList();

            return myList;
        }

        internal IEnumerable<SelectListItem> getSterrenbeelden(String sterrebeeld)
        {
            BuddiesDataContext dc = new BuddiesDataContext();
            List<SelectListItem> myListNew = new List<SelectListItem>();

            IEnumerable<SelectListItem> myListOld = from beelden in dc.bud_Sterrenbeeldens
                                                    select new SelectListItem
                                                    {
                                                        Text = beelden.Sterrenbeeld,
                                                        Value = beelden.Sterrenbeeld
                                                    };

            foreach (SelectListItem item in myListOld)
            {
                if (item.Text == sterrebeeld)
                {
                    item.Selected = true;
                }

                myListNew.Add(item);

            }

            myListNew = myListNew.OrderBy(p => p.Text).ToList();

            return myListNew;
        }

        internal IEnumerable<SelectListItem> getBurgelijkeStaten()
        {
            BuddiesDataContext dc = new BuddiesDataContext();

            IEnumerable<SelectListItem> myList = from staten in dc.bud_Burgerlijke_Statens
                                                 select new SelectListItem
                                                 {
                                                     Text = staten.Omschrijving,
                                                     Value = staten.Burgerlijke_staat_ID.ToString()
                                                 };

            myList = myList.OrderBy(p => p.Text).ToList();

            return myList;
        }

        internal IEnumerable<SelectListItem> getBurgelijkeStaten(String staat)
        {
            BuddiesDataContext dc = new BuddiesDataContext();
            List<SelectListItem> myListNew = new List<SelectListItem>();

            IEnumerable<SelectListItem> myListOld = from staten in dc.bud_Burgerlijke_Statens
                                                    select new SelectListItem
                                                    {
                                                        Text = staten.Omschrijving,
                                                        Value = staten.Burgerlijke_staat_ID.ToString()
                                                    };

            foreach (SelectListItem item in myListOld)
            {
                if (item.Text == staat)
                {
                    item.Selected = true;
                }

                myListNew.Add(item);

            }

            myListNew = myListNew.OrderBy(p => p.Text).ToList();

            return myListNew;
        }

        internal IEnumerable<SelectListItem> getStudieNiveaus()
        {
            BuddiesDataContext dc = new BuddiesDataContext();

            IEnumerable<SelectListItem> myList = from niveaus in dc.bud_Studieniveaus
                                                 select new SelectListItem
                                                 {
                                                     Text = niveaus.Omschrijving,
                                                     Value = niveaus.Studieniveau_ID.ToString()
                                                 };

            myList = myList.OrderBy(p => p.Text).ToList();

            return myList;
        }

        internal IEnumerable<SelectListItem> getStudieNiveaus(String niveau)
        {
            BuddiesDataContext dc = new BuddiesDataContext();
            List<SelectListItem> myListNew = new List<SelectListItem>();

            IEnumerable<SelectListItem> myListOld = from niveaus in dc.bud_Studieniveaus
                                                    select new SelectListItem
                                                    {
                                                        Text = niveaus.Omschrijving,
                                                        Value = niveaus.Studieniveau_ID.ToString()
                                                    };

            foreach (SelectListItem item in myListOld)
            {
                if (item.Text == niveau)
                {
                    item.Selected = true;
                }

                myListNew.Add(item);

            }

            myListNew = myListNew.OrderBy(p => p.Text).ToList();

            return myListNew;
        }


        internal IEnumerable<SelectListItem> getDagen()
        {
            List<SelectListItem> dagen = new List<SelectListItem>();

            for (int i = 1; i <= 31; i++)
            {
                dagen.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString() });
            }

            return dagen;
        }

        internal IEnumerable<SelectListItem> getDagen(DateTime geboorteDatum)
        {
            List<SelectListItem> dagen = new List<SelectListItem>();

            for (int i = 1; i <= 31; i++)
            {
                if (geboorteDatum.Day != i)
                {
                    dagen.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString() });
                }
                else
                {
                    dagen.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString(), Selected = true });
                }
            }

            return dagen;
        }

        internal IEnumerable<SelectListItem> getMaanden()
        {
            List<SelectListItem> maanden = new List<SelectListItem>();

            for (int i = 1; i <= 12; i++)
            {
                maanden.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString() });
            }

            return maanden;
        }

        internal IEnumerable<SelectListItem> getMaanden(DateTime geboorteDatum)
        {
            List<SelectListItem> maanden = new List<SelectListItem>();

            for (int i = 1; i <= 12; i++)
            {
                if (geboorteDatum.Month != i)
                {
                    maanden.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString() });
                }
                else
                {
                    maanden.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString(), Selected=true });
                }
                
                
            }

            return maanden;
        }

        internal IEnumerable<SelectListItem> getJaren()
        {
            List<SelectListItem> jaren = new List<SelectListItem>();

            for (int i = DateTime.Today.Year - 100; i <= DateTime.Today.Year; i++)
            {
                jaren.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString() });
            }

            return jaren;
        }

        internal IEnumerable<SelectListItem> getJaren(DateTime geboorteDatum)
        {
            List<SelectListItem> jaren = new List<SelectListItem>();

            for (int i = DateTime.Today.Year - 100; i <= DateTime.Today.Year; i++)
            {
                if (geboorteDatum.Year != i)
                {
                    jaren.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString() });
                }
                else
                {
                    jaren.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString(), Selected = true });
                }
                                
            }

            return jaren;
        }
    }
}