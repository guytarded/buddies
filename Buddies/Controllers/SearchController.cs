﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Buddies.Data_Access;
using Buddies.Models;

namespace Buddies.Controllers
{
    [Authorize]
    public class SearchController : Controller
    {
        // GET: Search
        public ActionResult Index()
        {
            var sv = new SearchValues();

           return View(sv);
        }

        // POST:
        [HttpPost]
        public ActionResult Search(FormCollection collection)
        {
            var dao = new ProfileDAO();

            // profileviewmodel via dao
            return View(dao.GetSearchResults(collection));
        }


    }
}