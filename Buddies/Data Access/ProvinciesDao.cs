﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using Buddies.Models;

namespace Buddies.Data_Access
{
    public class ProvinciesDao
    {
        private BuddiesDataContext Context { get; set; }

        public ProvinciesDao()
        {
            Context = new BuddiesDataContext();
        }

        /// <summary>
        /// Haalt een lijst op met provincies.
        /// </summary>
        /// <returns></returns>
        public List<bud_Provincy> GetAllProvincies()
        {
            using (var scope = new TransactionScope())
            {
                var query = (
                    from prov in Context.bud_Provincies
                    select prov).ToList<bud_Provincy>();

                scope.Complete();

                return query;
            }
        }
    }
}