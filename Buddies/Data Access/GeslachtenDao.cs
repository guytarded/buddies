﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using Buddies.Models;

namespace Buddies.Data_Access
{
    public class GeslachtenDao
    {
        private BuddiesDataContext Context { get; set; }

        public GeslachtenDao()
        {
            Context = new BuddiesDataContext();
        }

        /// <summary>
        /// Haalt een lijst op van alle geslachten
        /// </summary>
        /// <returns></returns>
        public List<bud_Geslachten> GetAllGeslachten()
        {
            using (var scope = new TransactionScope())
            {
                var query = (
                    from gs in Context.bud_Geslachtens
                    select gs).ToList<bud_Geslachten>();

                scope.Complete();

                return query;
            }
        }
    }
}