﻿using Buddies.Data_Access;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Buddies.Models
{
    public class CreateViewModel
    {
        public String UserID { get; set; }
        public String UserName { get; set; }
        public IEnumerable<SelectListItem> Landen { get; set; }
        public IEnumerable<SelectListItem> Geslacht { get; set; }
        public IEnumerable<SelectListItem> Sterrenbeeld { get; set; }
        public String Beroep { get; set; }
        public IEnumerable<SelectListItem> BurgerlijkeStaat { get; set; }
        public IEnumerable<SelectListItem> Dagen { get; set; }
        public IEnumerable<SelectListItem> Maanden { get; set; }
        public IEnumerable<SelectListItem> Jaren { get; set; }
        public IEnumerable<SelectListItem> StudieNiveau { get; set; }
        public String Grootte { get; set; }
        public String Gewicht { get; set; }
        public String  Hobbys { get; set; }

        public CreateViewModel(RegisterViewModel model)
        {
            SelectListDAO _dao = new SelectListDAO();
            ProfileDAO _profileDao = new ProfileDAO();

            this.Landen = _dao.getCountries();
            this.Geslacht = _dao.getGeslachten();
            this.UserName = model.UserName;
            this.Sterrenbeeld = _dao.getSterrenbeelden();
            this.UserID = _profileDao.getIdByUserName(model.UserName);
            this.BurgerlijkeStaat = _dao.getBurgelijkeStaten();
            this.StudieNiveau = _dao.getStudieNiveaus();
            this.Dagen = _dao.getDagen();
            this.Maanden = _dao.getMaanden();
            this.Jaren = _dao.getJaren();
        }
    }

    public class ProfileViewModel
    {
        [Key]
        public String UserID { get; set; }
        public String UserName { get; set; }
        public String Land { get; set; }
        public String Geslacht { get; set; }
        public DateTime GeboorteDatum { get; set; }
        public String Sterrenbeeld { get; set; }
        public String Beroep { get; set; }
        public String BurgerlijkeStaat { get; set; }
        public String StudieNiveau { get; set; }
        public String Grootte { get; set; }
        public String Gewicht { get; set; }
        public String Hobbys { get; set; }
        public bool IsActief { get; set; }

        public ProfileViewModel()
        {
            
        }
    }

    public class UpdateProfileViewModel
    {
        public String UserID { get; set; }
        public String UserName { get; set; }
        public IEnumerable<SelectListItem> Landen { get; set; }
        public IEnumerable<SelectListItem> Geslacht { get; set; }
        public DateTime GeboorteDatum { get; set; }
        public IEnumerable<SelectListItem> Sterrenbeeld { get; set; }
        public String Beroep { get; set; }
        public IEnumerable<SelectListItem> BurgerlijkeStaat { get; set; }
        public IEnumerable<SelectListItem> StudieNiveau { get; set; }
        public String Grootte { get; set; }
        public String Gewicht { get; set; }
        public String Hobbys { get; set; }
        public IEnumerable<SelectListItem> Dagen { get; set; }
        public IEnumerable<SelectListItem> Maanden { get; set; }
        public IEnumerable<SelectListItem> Jaren { get; set; }

        public UpdateProfileViewModel()
        {
            
        }
    }
}