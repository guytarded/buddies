﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using Buddies.Models;

namespace Buddies.Data_Access
{
    public class PersoonlijkhedenDao
    {
        private BuddiesDataContext Context { get; set; }

        public PersoonlijkhedenDao()
        {
            Context = new BuddiesDataContext();
        }

        /// <summary>
        /// Haalt een lijst op van persoonlijkheden.
        /// </summary>
        /// <returns></returns>
        public List<bud_Persoonlijkheden> GetAllPersoonlijkheden()
        {
            using (var scope = new TransactionScope())
            {
                var query = (
                    from pers in Context.bud_Persoonlijkhedens
                    select pers).ToList<bud_Persoonlijkheden>();

                scope.Complete();

                return query;
            }
        }
    }
}