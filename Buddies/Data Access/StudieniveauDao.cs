﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using Buddies.Models;

namespace Buddies.Data_Access
{
    public class StudieniveauDao
    {
        private BuddiesDataContext Context { get; set; }

        public StudieniveauDao()
        {
            Context = new BuddiesDataContext();
        }

        /// <summary>
        /// Haalt een lijst met alle studieniveaus op.
        /// </summary>
        /// <returns></returns>
        public List<bud_Studieniveaus> GetAllStudieniveaus()
        {
            using (var scope = new TransactionScope())
            {
                var query = (
                    from std in Context.bud_Studieniveaus
                    select std).ToList<bud_Studieniveaus>();

                scope.Complete();

                return query;
            }
        }
    }
}