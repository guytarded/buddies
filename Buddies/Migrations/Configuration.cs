namespace Buddies.Migrations
{
    using Buddies.Models;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Validation;
    using System.Linq;
    using System.Text;

    internal sealed class Configuration : DbMigrationsConfiguration<Buddies.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            ContextKey = "Buddies.Models.ApplicationDbContext";
        }

        protected override void Seed(Buddies.Models.ApplicationDbContext context)
        {
            try
            {
                List<Groep> myRoles = GetTestRolen();

                foreach (Groep rol in myRoles)
                {
                    if (!context.Roles.Any(r => r.Name == rol.Naam))
                    {
                        var userRole = new IdentityRole { Name = rol.Naam, Id = rol.Id.ToString() };//Guid.NewGuid().ToString() };
                        context.Roles.Add(userRole);
                    }
                }

                List<ApplicationUser> myUsers = GetTestUsers();
                var passwordHasher = new PasswordHasher();

                foreach (var user in myUsers)
                {
                    if (!context.Users.Any(r => r.UserName == user.UserName))
                    {
                        List<int> usersRoles = GetUserRoles(user.UserName);
                        foreach (int roleId in usersRoles)
                        {
                            var userRole = new IdentityRole { Name = "Klant" };
                            user.Roles.Add(new IdentityUserRole { RoleId = roleId.ToString(), UserId = user.Id });
                        };
                        context.Users.Add(user);
                    }
                }
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception("Error Add User: " + ex.Message + "\n" + ex.InnerException);
            }
            finally
            {
                base.Seed(context);
            }
        }

        private void SaveChanges(DbContext context)
        {
            try
            {
                context.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                StringBuilder sb = new StringBuilder();

                foreach (var failure in ex.EntityValidationErrors)
                {
                    sb.AppendFormat("{0} failed validation\n", failure.Entry.Entity.GetType());
                    foreach (var error in failure.ValidationErrors)
                    {
                        sb.AppendFormat("- {0} : {1}", error.PropertyName, error.ErrorMessage);
                        sb.AppendLine();
                    }
                }

                throw new DbEntityValidationException(
                    "Entity Validation Failed - errors follow:\n" +
                    sb.ToString(), ex
                ); // Add the original exception as the innerException
            }
        }

        #region Helpers
        private List<ApplicationUser> GetTestUsers()
        {
            var hasher = new PasswordHasher();

            List<ApplicationUser> testUsers = new List<ApplicationUser>
                {
                    new ApplicationUser
                    {
                        Id = "1",
                        UserName = "Admin",
                        PasswordHash = hasher.HashPassword("123123"),
                        SecurityStamp = Guid.NewGuid().ToString()
                    },
                    new ApplicationUser
                    {
                        Id = "2",
                        UserName = "John",
                        PasswordHash = hasher.HashPassword("123123"),
                        SecurityStamp = Guid.NewGuid().ToString()
                    },
                    new ApplicationUser
                    {
                        Id = "3",
                        UserName = "Jim",
                        PasswordHash = hasher.HashPassword("123123"),
                        SecurityStamp = Guid.NewGuid().ToString()
                    },

                };
            return testUsers;
        }

        public List<Groep> GetTestRolen()
        {
            List<Groep> myRoles = new List<Groep>
            {   new Groep
                {
                    Id = 1, Naam = "Admin"
                },
                new Groep
                {
                    Id = 2, Naam = "Klant"
                }
            };
            return myRoles;
        }

        public List<int> GetUserRoles(string user)
        {
            List<int> myRoles = new List<int>();
            switch (user)
            {
                case "Admin":
                    myRoles = new List<int>(new int[] { 1, 2 });
                    break;
                case "John":
                    myRoles = new List<int>(new int[] { 2 });
                    break;
                case "Jim":
                    myRoles = new List<int>(new int[] { 2 });
                    break;
                default:
                    myRoles = new List<int>(new int[] { 0 });
                    break;
            }
            return myRoles;
        }

        #endregion

        public class Groep
        {
            public int Id { get; set; }
            public string Naam { get; set; }
        }
    }
}
