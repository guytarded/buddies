﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using Buddies.Models;

namespace Buddies.Data_Access
{
    public class OogkleurenDao
    {
        private BuddiesDataContext Context { get; set; }

        public OogkleurenDao()
        {
            Context = new BuddiesDataContext();
        }

        /// <summary>
        /// Haalt een lijst van alle oogkleuren op
        /// </summary>
        /// <returns></returns>
        public List<bud_OogKleuren> GetAllOogKleuren()
        {
            using (var scope = new TransactionScope())
            {
                var query = (
                    from oogkl in Context.bud_OogKleurens
                    select oogkl).ToList<bud_OogKleuren>();

                scope.Complete();

                return query;
            }
        }
    }
}