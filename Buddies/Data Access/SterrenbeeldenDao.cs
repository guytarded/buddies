﻿using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using Buddies.Models;

namespace Buddies.Data_Access
{
    public class SterrenbeeldenDao
    {
        private BuddiesDataContext Context { get; set; }

        public SterrenbeeldenDao()
        {
            Context = new BuddiesDataContext();
        }

        /// <summary>
        /// Haalt alle sterrenbeelden op.
        /// </summary>
        /// <returns></returns>
        public List<bud_Sterrenbeelden> GetAllSterrenbeelden()
        {
            using (var scope = new TransactionScope())
            {
                var query = (
                    from sb in Context.bud_Sterrenbeeldens
                    select sb).ToList<bud_Sterrenbeelden>();

                scope.Complete();

                return query;
            }
        }
    }
}