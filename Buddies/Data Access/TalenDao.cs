﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using Buddies.Models;

namespace Buddies.Data_Access
{
    public class TalenDao
    {
        private BuddiesDataContext Context { get; set; }

        public TalenDao()
        {
            Context = new BuddiesDataContext();
        }

        /// <summary>
        /// Haalt een lijst van talen op.
        /// </summary>
        /// <returns></returns>
        public List<bud_Talen> GetAllTalen()
        {
            using (var scope = new TransactionScope())
            {
                var query = (
                    from taal in Context.bud_Talens
                    select taal).ToList<bud_Talen>();

                scope.Complete();

                return query;
            }
        }
    }
}