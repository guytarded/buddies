﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Buddies.Data_Access;

namespace Buddies.Models
{
    public class SearchViewModel
    {
        public List<ProfileViewModel> Profiles { get; set; }

        public SearchViewModel()
        {
            Profiles = new List<ProfileViewModel>();
        }
    }
}