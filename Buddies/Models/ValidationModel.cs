﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Buddies.Models
{
    public class ValidationModel
    {
        [Key]
        public int Id { get; set; }
        [Required(ErrorMessage="Naam is verplciht")]
        public string Naam { get; set; }
        [Required(ErrorMessage="E-mail is verplicht")]
        [RegularExpression(@"^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$", ErrorMessage="Geen geldig e-mail adres.") ]
        public string Email { get; set; }
        [Range(10, 99, ErrorMessage="Leeftijd moet tussen 10 en 99 zijn.")]
        public int Leeftijd { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        [Compare("Password", ErrorMessage="Paswoorden komen niet overeen")]
        public string ConfirmPassword { get; set; }
    }
}