﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using Buddies.Models;

namespace Buddies.Data_Access
{
    public class BurgStaatDao
    {
        private BuddiesDataContext Context { get; set; }

        public BurgStaatDao()
        {
            Context = new BuddiesDataContext();
        }

        /// <summary>
        /// Haalt een lijst op van alle burgerlijke staten.
        /// </summary>
        /// <returns></returns>
        public List<bud_Burgerlijke_Staten> GetAllBurgerlijkeStaten()
        {
            using (var scope = new TransactionScope())
            {
                var query = (
                    from bs in Context.bud_Burgerlijke_Statens
                    select bs).ToList<bud_Burgerlijke_Staten>();

                scope.Complete();

                return query;
            }
        }
    }
}