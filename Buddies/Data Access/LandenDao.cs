﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using Buddies.Models;

namespace Buddies.Data_Access
{
    public class LandenDao
    {
        private BuddiesDataContext Context { get; set; }

        public LandenDao()
        {
            Context = new BuddiesDataContext();
        }

        /// <summary>
        /// Haalt een lijst op van alle landen.
        /// </summary>
        /// <returns></returns>
        public List<bud_Landen> GetAllLanden()
        {
            using (var scope = new TransactionScope())
            {
                var query = (
                    from lnd in Context.bud_Landens
                    select lnd).ToList<bud_Landen>();

                scope.Complete();

                return query;
            }
        }
    }
}