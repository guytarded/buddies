﻿using Buddies.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Microsoft.AspNet.Identity;

namespace Buddies.Data_Access
{
    public class ProfileDAO
    {
        BuddiesDataContext dc = new BuddiesDataContext();

        public String getIdByUserName(String id)
        {
            String Id = (from users in dc.AspNetUsers
                         where users.UserName == id
                         select users.Id).First();
            return Id;
        }

        internal void createProfile(FormCollection form)
        {
            bud_Profielen mijnProfiel = new bud_Profielen();
            int dagen = Convert.ToInt32(form["Dagen"]);
            int maanden = Convert.ToInt32(form["Maanden"]);
            int jaren = Convert.ToInt32(form["Jaren"]);
            DateTime geboorteDatum = new DateTime(jaren, maanden, dagen);

            

            mijnProfiel.GeboorteDatum = geboorteDatum;

            mijnProfiel.Abonnee_ID = form["UserID"];
            mijnProfiel.Naam_Nickname = form["UserName"];
            mijnProfiel.Land_ID = Convert.ToInt32(form["Landen"]);
            mijnProfiel.Geslacht = form["Geslacht"];
            mijnProfiel.Sterrenbeeld = form["Sterrenbeeld"];
            mijnProfiel.Beroep = form["Beroep"];
            mijnProfiel.Burgerlijke_staat_ID = Convert.ToInt32(form["BurgerlijkeStaat"]);
            mijnProfiel.Studieniveau_ID = Convert.ToInt32(form["StudieNiveau"]);
            mijnProfiel.Grootte = form["Grootte"];
            mijnProfiel.Gewicht = form["Gewicht"];
            mijnProfiel.Hobbys = form["Hobbys"];
            mijnProfiel.IsActief = true;

            dc.bud_Profielens.InsertOnSubmit(mijnProfiel);
            dc.SubmitChanges();

            // assign Klant role to user
            var _context = new ApplicationDbContext();
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(_context));
            UserManager.AddToRole(mijnProfiel.Abonnee_ID, "Klant");
        }

        /// <summary>
        /// Get all profiles
        /// </summary>
        /// <returns>A SearchViewModel</returns>
        public SearchViewModel GetAllProfiles()
        {
            var model = new SearchViewModel();
            var results = (from prof in dc.bud_Profielens select prof).ToList<bud_Profielen>();
            
            foreach (var pvm in results.Select(profile => new ProfileViewModel
            {
                Beroep = profile.Beroep,
                BurgerlijkeStaat = (from bs in dc.bud_Burgerlijke_Statens
                                    where bs.Burgerlijke_staat_ID == profile.Burgerlijke_staat_ID
                                    select bs.Omschrijving).First().ToString(),
                GeboorteDatum = (DateTime)profile.GeboorteDatum,
                Geslacht = profile.Geslacht,
                Gewicht = profile.Gewicht,
                Grootte = profile.Grootte,
                Hobbys = profile.Hobbys,
                Land = (from lnd in dc.bud_Landens where lnd.Land_ID == profile.Land_ID select lnd.Land).First().ToString(),
                Sterrenbeeld = profile.Sterrenbeeld,
                StudieNiveau = (from stud in dc.bud_Studieniveaus
                                where stud.Studieniveau_ID == profile.Studieniveau_ID
                                select stud.Omschrijving).First().ToString(),
                UserName = profile.Naam_Nickname,
                UserID = profile.Abonnee_ID,
                IsActief = profile.IsActief
            }))
            {
                model.Profiles.Add(pvm);
            }

            return model;
        }

        public ProfileViewModel getProfileById(String id)
        {
            ProfileViewModel mijnProfiel = new ProfileViewModel();

            var query = (from u in dc.bud_Profielens
                         where u.Abonnee_ID == id
                         select u).First();

            mijnProfiel.UserID = id;
            mijnProfiel.UserName = query.Naam_Nickname;
            mijnProfiel.Land = query.bud_Landen.Land;
            mijnProfiel.Geslacht = query.bud_Geslachten.Geslacht;
            mijnProfiel.GeboorteDatum = (DateTime)query.GeboorteDatum;
            mijnProfiel.Sterrenbeeld = query.bud_Sterrenbeelden.Sterrenbeeld;
            mijnProfiel.Beroep = query.Beroep;
            mijnProfiel.BurgerlijkeStaat = query.bud_Burgerlijke_Staten.Omschrijving;
            mijnProfiel.StudieNiveau = query.bud_Studieniveaus.Omschrijving;
            mijnProfiel.Grootte = query.Grootte;
            mijnProfiel.Gewicht = query.Gewicht;
            mijnProfiel.Hobbys = query.Hobbys;
            mijnProfiel.IsActief = query.IsActief;

            return mijnProfiel;
        }

        internal UpdateProfileViewModel createUpdateProfile(ProfileViewModel model)
        {
            SelectListDAO _dao = new SelectListDAO();
            UpdateProfileViewModel mijnProfiel = new UpdateProfileViewModel();

            mijnProfiel.UserID = model.UserID;
            mijnProfiel.UserName = model.UserName;
            mijnProfiel.Landen = _dao.getCountries(model.Land);
            mijnProfiel.Geslacht = _dao.getGeslachten(model.Geslacht);
            mijnProfiel.Sterrenbeeld = _dao.getSterrenbeelden(model.Sterrenbeeld);
            mijnProfiel.Beroep = model.Beroep;
            mijnProfiel.BurgerlijkeStaat = _dao.getBurgelijkeStaten(model.BurgerlijkeStaat);
            mijnProfiel.StudieNiveau = _dao.getStudieNiveaus(model.StudieNiveau);
            mijnProfiel.Grootte = model.Grootte;
            mijnProfiel.Gewicht = model.Gewicht;
            mijnProfiel.Hobbys = model.Hobbys;
            mijnProfiel.Dagen = _dao.getDagen(model.GeboorteDatum);
            mijnProfiel.Maanden = _dao.getMaanden(model.GeboorteDatum);
            mijnProfiel.Jaren = _dao.getJaren(model.GeboorteDatum);

            return mijnProfiel;
        }

        internal void updateProfile(FormCollection form)
        {
            var mijnProfiel = (from u in dc.bud_Profielens
                               where u.Abonnee_ID == form["UserID"]
                               select u).First();

            int dagen = Convert.ToInt32(form["Dagen"]);
            int maanden = Convert.ToInt32(form["Maanden"]);
            int jaren = Convert.ToInt32(form["Jaren"]);
            DateTime geboorteDatum = new DateTime(jaren, maanden, dagen);

            mijnProfiel.GeboorteDatum = geboorteDatum;

            mijnProfiel.Abonnee_ID = form["UserID"];
            mijnProfiel.Naam_Nickname = form["UserName"];
            mijnProfiel.Land_ID = Convert.ToInt32(form["Land"]);
            mijnProfiel.Geslacht = form["Geslacht"];
            mijnProfiel.Sterrenbeeld = form["Sterrenbeeld"];
            mijnProfiel.Beroep = form["Beroep"];
            mijnProfiel.Burgerlijke_staat_ID = Convert.ToInt32(form["BurgerlijkeStaat"]);
            mijnProfiel.Studieniveau_ID = Convert.ToInt32(form["StudieNiveau"]);
            mijnProfiel.Grootte = form["Grootte"];
            mijnProfiel.Gewicht = form["Gewicht"];
            mijnProfiel.Hobbys = form["Hobbies"];

            dc.SubmitChanges();
        }

        public SearchViewModel GetSearchResults(FormCollection collection)
        {
            string username = collection["UserName"];
            string geslacht = collection["Geslacht"];
            string beroep = collection["Beroep"];
            string sterrenbeeld = collection["SterrenBeeld"];
            int burgstaat, land, studieniveau;
            try
            {
                burgstaat = int.Parse(collection["BurgerlijkeStaat"]);
                land = int.Parse(collection["Landen"]);
                studieniveau = int.Parse(collection["StudieNiveau"]);
            }
            catch (FormatException)
            {
                burgstaat = 0;
                land = 0;
                studieniveau = 0;
            }

            // query
            var results = (from pro in dc.bud_Profielens where pro.IsActief select pro);
            var adminRoleId = (from rl in dc.AspNetRoles where rl.Name == "Admin" select rl.Id).First();

            // filter admins from results
            var admins = (from adm in dc.AspNetUserRoles where adm.RoleId == adminRoleId select adm).ToList();
            results = admins.Aggregate(results, (current, admin) => current.Where(x => x.Abonnee_ID != admin.UserId));

            // drilldown
            if (collection["UserName"] != string.Empty)
                results = results.Where(x => x.Naam_Nickname == username);

            if (collection["Geslacht"] != string.Empty)
                results = results.Where(x => x.Geslacht == geslacht);

            if (burgstaat != 0)
            {
                results = results.Where(x => x.Burgerlijke_staat_ID == burgstaat);
            }

            if (land != 0)
            {
                results = results.Where(x => x.Land_ID == land);
            }

            if (studieniveau != 0)
            {
                results = results.Where(x => x.Studieniveau_ID == studieniveau);
            }

            if (collection["Beroep"] != string.Empty)
                results = results.Where(x => x.Beroep == beroep);

            if (collection["Sterrenbeeld"] != string.Empty)
                results = results.Where(x => x.Sterrenbeeld == sterrenbeeld);

            // init
            var model = new SearchViewModel();

            foreach (var pvm in results.Select(profile => new ProfileViewModel
            {
                Beroep = profile.Beroep,
                BurgerlijkeStaat = (from bs in dc.bud_Burgerlijke_Statens
                    where bs.Burgerlijke_staat_ID == profile.Burgerlijke_staat_ID
                    select bs.Omschrijving).First().ToString(),
                GeboorteDatum = (DateTime) profile.GeboorteDatum,
                Geslacht = profile.Geslacht,
                Gewicht = profile.Gewicht,
                Grootte = profile.Grootte,
                Hobbys = profile.Hobbys,
                Land = (from lnd in dc.bud_Landens where lnd.Land_ID == profile.Land_ID select lnd.Land).First().ToString(),
                Sterrenbeeld = profile.Sterrenbeeld,
                StudieNiveau = (from stud in dc.bud_Studieniveaus
                    where stud.Studieniveau_ID == profile.Studieniveau_ID
                    select stud.Omschrijving).First().ToString(),
                UserName = profile.Naam_Nickname,
                UserID = profile.Abonnee_ID
            }))
            {
                model.Profiles.Add(pvm);
            }

            return model;
        }

        /// <summary>
        /// Toggle activationstatus
        /// </summary>
        /// <param name="id"></param>
        [Authorize(Roles = "Admin")]
        public int ToggleProfileActivation(string id)
        {
            try
            {
                // get profile
                var prof = (from profile in dc.bud_Profielens where profile.Abonnee_ID == id select profile).First();

                // update
                prof.IsActief = !prof.IsActief;

                dc.SubmitChanges();

                return 1;
            }
            catch (Exception)
            {
                return 0;
            }
        }
    }
}