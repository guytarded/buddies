﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using Buddies.Models;

namespace Buddies.Data_Access
{
    public class InteressesDao
    {
        private BuddiesDataContext Context { get; set; }

        public InteressesDao()
        {
            Context = new BuddiesDataContext();
        }

        /// <summary>
        /// Haalt een lijst met interesses op.
        /// </summary>
        /// <returns></returns>
        public List<bud_Interess> GetAllInteresses()
        {
            using (var scope = new TransactionScope())
            {
                var query = (
                    from inter in Context.bud_Interesses
                    select inter).ToList<bud_Interess>();

                scope.Complete();

                return query;
            }
        }
    }
}