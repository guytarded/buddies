﻿using Buddies.Data_Access;
using Buddies.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Buddies.Controllers
{
    public class ProfileController : Controller
    {
        // GET: Profile
        public ActionResult Index()
        {
            return View();
        }

        // GET: Profile/Details/5
        public ActionResult Details(String id)
        {
            ProfileDAO _dao = new ProfileDAO();
            ProfileViewModel mijnProfiel = new ProfileViewModel();
            mijnProfiel = _dao.getProfileById(id);
            
            return View(mijnProfiel);
        }

        // GET: Profile/Create
        public ActionResult Create(RegisterViewModel model)
        { 
            CreateViewModel myCreateViewModel = new CreateViewModel(model);

            return View(myCreateViewModel);
        }

        // POST: Profile/Create
        [HttpPost]
        public ActionResult Create(FormCollection form)
        {
            ProfileDAO _dao = new ProfileDAO();

            _dao.createProfile(form);

            return RedirectToAction("Details", new { id = form["UserID"] });
        }

        // GET: Profile/Edit/5
        public ActionResult Edit(String id)
        {
            ProfileDAO _profileDao = new ProfileDAO();
            UpdateProfileViewModel mijnNieuwModel = new UpdateProfileViewModel();                        
            ProfileViewModel mijnProfiel = new ProfileViewModel();

            mijnProfiel = _profileDao.getProfileById(id);

            mijnNieuwModel = _profileDao.createUpdateProfile(mijnProfiel);
            
            
            return View(mijnNieuwModel);
        }

        // POST: Profile/Edit/5
        [HttpPost]
        public ActionResult Edit(FormCollection collection)
        {
            try
            {
                ProfileDAO _dao = new ProfileDAO();

                _dao.updateProfile(collection);

                return RedirectToAction("Details", new { id = collection["UserID"] });
            }
            catch
            {
                return View();
            }
        }

        // GET: Profile/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Profile/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
