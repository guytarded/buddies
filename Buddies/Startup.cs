﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Buddies.Startup))]
namespace Buddies
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
