﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using Buddies.Models;

namespace Buddies.Data_Access
{
    public class PostcodeDao
    {
        private BuddiesDataContext Context { get; set; }

        public PostcodeDao()
        {
            Context = new BuddiesDataContext();
        }

        /// <summary>
        /// Haalt een lijst van alle postcodes op.
        /// </summary>
        /// <returns></returns>
        public List<bud_Postcode> GetAllPostcodes()
        {
            using (var scope = new TransactionScope())
            {
                var query = (
                    from pc in Context.bud_Postcodes
                    select pc).ToList<bud_Postcode>();

                scope.Complete();

                return query;
            }
        }
    }
}